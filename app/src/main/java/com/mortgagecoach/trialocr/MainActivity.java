package com.mortgagecoach.trialocr;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ToggleButton;

import com.googlecode.leptonica.android.AdaptiveMap;
import com.googlecode.leptonica.android.Enhance;
import com.googlecode.leptonica.android.Pix;
import com.googlecode.leptonica.android.ReadFile;
import com.googlecode.leptonica.android.Rotate;
import com.googlecode.leptonica.android.Scale;
import com.googlecode.leptonica.android.WriteFile;
import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MainActivity extends ActionBarActivity {

    public static final String DATA_PATH = Environment
            .getExternalStorageDirectory().toString() + "/TrialOCR/";

    // You should have the trained data file in assets folder
    // You can get them at:
    // http://code.google.com/p/tesseract-ocr/downloads/list
    public static final String lang = "eng";

    private static final String TAG = "SVK";

    protected Button _button;
    //protected ImageView _image;
    protected EditText _field;
    protected String _path;
    private String recognizedText = "";
    private ProgressDialog mProgressDialog = null;

    protected static final String PHOTO_TAKEN = "photo_taken";

    @Override
    public void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        String[] paths = new String[] { DATA_PATH, DATA_PATH + "tessdata/" };

        for (String path : paths)
        {
            File dir = new File(path);
            if (!dir.exists())
            {
                if (!dir.mkdirs())
                {
                    Log.v(TAG, "ERROR: Creation of directory " + path + " on sdcard failed");
                    return;
                } else
                {
                    Log.v(TAG, "Created directory " + path + " on sdcard");
                }
            }
        }

        // lang.traineddata file with the app (in assets folder)
        // You can get them at:
        // http://code.google.com/p/tesseract-ocr/downloads/list
        // This area needs work and optimization
        if (!(new File(DATA_PATH + "tessdata/" + lang + ".traineddata")).exists())
        {
            try
            {
                AssetManager assetManager = getAssets();
                InputStream in = assetManager.open("tessdata/" + lang + ".traineddata");
                //GZIPInputStream gin = new GZIPInputStream(in);
                OutputStream out = new FileOutputStream(DATA_PATH
                        + "tessdata/" + lang + ".traineddata");
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                //while ((lenf = gin.read(buff)) > 0) {
                while ((len = in.read(buf)) > 0)
                {
                    out.write(buf, 0, len);
                }
                in.close();
                //gin.close();
                out.close();

                Log.v(TAG, "Copied " + lang + " traineddata");
            }
            catch (IOException e)
            {
                Log.e(TAG, "Was unable to copy " + lang + " traineddata " + e.toString());
            }
        }

        //_image = (ImageView) findViewById(R.id.processed_image);
        _field = (EditText) findViewById(R.id.field);
        _button = (Button) findViewById(R.id.button);
        _button.setOnClickListener(new ButtonClickHandler());
        _path = DATA_PATH + "/ocr.jpg";
    }

    public class ButtonClickHandler implements View.OnClickListener {
        public void onClick(View view) {
            Log.v(TAG, "Starting Camera app");
            startCameraActivity();
            //onPhotoTaken();
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        dismissProgressDialog();
    }

    private void dismissProgressDialog()
    {
        if (mProgressDialog != null && mProgressDialog.isShowing())
        {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    // Simple android photo capture:
    // http://labs.makemachine.net/2010/03/simple-android-photo-capture/

    protected void startCameraActivity() {
        Intent intent = new Intent(this, CameraActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.i(TAG, "resultCode: " + resultCode);

        if (resultCode == -1) {
            Button interpret = (Button) findViewById(R.id.btn_interpret);
            interpret.setVisibility(View.GONE);
            onPhotoTaken();
        } else {
            Log.v(TAG, "User cancelled");
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.i(TAG, "onRestoreInstanceState()");
        if (savedInstanceState.getBoolean(PHOTO_TAKEN)) {
            onPhotoTaken();
        }
    }

    protected void onPhotoTaken() {

        mProgressDialog = ProgressDialog.show(this, null, "Processing image, please wait...", true, true);
        Worker.doWork(new Runnable() {
            @Override
            public void run() {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 1;
                options.inMutable = true;

                Bitmap bitmap = BitmapFactory.decodeFile(_path, options);

                Pix pix = ReadFile.readBitmap(bitmap);
                bitmap.recycle();
                Log.d("SVK", "bitmap size=" + pix.getWidth() + "," + pix.getHeight());

                //if the image is landscape, rotate it 1 quadrant
                if (pix.getWidth() > pix.getHeight()) {
                    pix = Rotate.rotateOrth(pix, 1);
                    Log.d("SVK", "after rotate");
                }

                Bitmap bmp = WriteFile.writeBitmap(pix);
                TessBaseAPI baseApi = new TessBaseAPI();
                baseApi.setDebug(true);
                baseApi.init(DATA_PATH, lang);
                baseApi.setImage(bmp);
                baseApi.setVariable(TessBaseAPI.VAR_CHAR_WHITELIST, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-,.$'%+/#&?");


                recognizedText = baseApi.getUTF8Text();
                baseApi.end();
                Worker.doWorkOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        Button interpret = (Button) findViewById(R.id.btn_interpret);
                        interpret.setVisibility(View.VISIBLE);
                        _field.setVisibility(View.VISIBLE);
                        _field.setText(recognizedText);
                        dismissProgressDialog();
                    }
                });
                //bmp.recycle();
                Log.d(TAG, "OCR routine finished !!!!");

            }
        });
    }

    public void onInterpret(View v)
    {
        final List<String> finalLines = new ArrayList<String>();
        int startCopying = 0;
        try {
            Scanner scanner = new Scanner(recognizedText).useDelimiter("\n");
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                line = line.trim();
                line = line.replaceAll("\\s+", " ");
                finalLines.add(line);
                /*String lineTitle = line.substring(0, line.length() >= 13 ? 13 : line.length()).toUpperCase();
                if (levenshteinDistance(lineTitle, "LOAN ESTIMATE") < 3) //trying with 5 for now
                {
                    startCopying++;
                }
                if (startCopying == 1) {
                    finalLines.add(line);
                } else if (startCopying == 2) {
                    break;
                }*/
            }
            scanner.close();
            Worker.doWorkOnMainThread(new Runnable() {
                @Override
                public void run() {
                    handleLines(finalLines);
                    _field.setVisibility(View.VISIBLE);
                    Button interpret = (Button) findViewById(R.id.btn_interpret);
                    interpret.setVisibility(View.GONE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleLines(List<String> lines)
    {
        StringBuilder builder = new StringBuilder();

        // line 0 is loan estimate, loan terms
        String expectedLine0Str = "Loan Estimate LOAN TERM "; //trailing space
        String expectedLine0Pattern = "(\\D+)(\\d*)\\D*";
        String closestLine = findClosestMatchingLine(lines, 0, expectedLine0Str);
        Matcher line0 = Pattern.compile(expectedLine0Pattern).matcher(closestLine);
        if (line0.matches())
        {
            //handle line 1
            if (line0.groupCount() == 2)
            {
                builder.append("Loan Term = " + line0.group(2) + "\n");
            }
        }

        // line 10 is loan amount
        String expectedLine10Str = "Loan Amount $";
        String expectedLine10Pattern = "(\\D+)([\\d,'\\s]*)\\w*";
        closestLine = findClosestMatchingLine(lines, 10, expectedLine10Str);
        Matcher line10 = Pattern.compile(expectedLine10Pattern).matcher(closestLine);
        if (line10.matches())
        {
            //handle line 10
            if (line10.groupCount() == 2)
            {
                builder.append("Loan Amount = " + line10.group(2) + "\n");
            }
        }

        // line 11 is interest rate
        String expectedLine11Str = "Interest Rate "; //trailing space
        String expectedLine11Pattern = "(\\D+)(\\d+\\D?\\d+).*";
        closestLine = findClosestMatchingLine(lines, 11, expectedLine11Str);
        Matcher line11 = Pattern.compile(expectedLine11Pattern).matcher(closestLine);
        if (line11.matches())
        {
            //handle line 11
            if (line11.groupCount() == 2)
            {
                builder.append("Interest Rate = " + line11.group(2) + "\n");
            }
        }

        // line 12 is interest rate
        String expectedLine12Str = "Monthly Principal & Interest $";
        String expectedLine12Pattern = "(.{25,}?)(\\d+.\\d+)\\D[\\D0]*";
        closestLine = findClosestMatchingLine(lines, 12, expectedLine12Str);
        Matcher line12 = Pattern.compile(expectedLine12Pattern).matcher(closestLine);
        if (line12.matches())
        {
            //handle line 11
            if (line12.groupCount() == 2)
            {
                builder.append("Interest and principal =  " + line12.group(2) + "\n");
            }
        }

        _field.setText(builder.toString());
    }

    /* this routine loops thru 7 lines (3 before and 3 after the lineNo), to find the one
    that is the closest match for the expected text we expect and uses that line.
    Handles edge cases if three before and three after would be before/after the end of the array
     */
    private String findClosestMatchingLine(List<String> lines, int lineNo, String expectedText)
    {
        int closestMatchLineNo = -1;
        int closestDistance = 999;
        int start = lineNo - 3;
        if (start < 0)
            start = 0;
        int end = lineNo + 3;
        if (end >= lines.size())
            end = lines.size()-1;
        for(int i=start; i<=end; i++)
        {
            String currentLine = lines.get(i);
            int count = (currentLine.length() >= expectedText.length() ? expectedText.length() : currentLine.length());

            int distance = levenshteinDistance(expectedText, currentLine.substring(0, count));
            if (distance < closestDistance)
            {
                closestDistance = distance;
                closestMatchLineNo = i;
            }
        }
        Log.d("SVK", expectedText + " : closest matching = " + closestMatchLineNo);
        if (closestMatchLineNo == -1)
        {
            return lines.get(lineNo);
        }
        return lines.get(closestMatchLineNo);
    }


    public static int levenshteinDistance(String a, String b) {
        a = a.toLowerCase();
        b = b.toLowerCase();
        // i == 0
        int [] costs = new int [b.length() + 1];
        for (int j = 0; j < costs.length; j++)
            costs[j] = j;
        for (int i = 1; i <= a.length(); i++) {
            // j == 0; nw = lev(i - 1, j)
            costs[0] = i;
            int nw = i - 1;
            for (int j = 1; j <= b.length(); j++) {
                int cj = Math.min(1 + Math.min(costs[j], costs[j - 1]), a.charAt(i - 1) == b.charAt(j - 1) ? nw : nw + 1);
                nw = costs[j];
                costs[j] = cj;
            }
        }
        return costs[b.length()];
    }
}
