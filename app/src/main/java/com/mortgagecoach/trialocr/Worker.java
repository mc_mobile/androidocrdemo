package com.mortgagecoach.trialocr;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

/**
 * Created by sanjivkarnataki on 4/27/15.
 */
public class Worker extends HandlerThread
{


    public static synchronized void doWork(Runnable work)
    {
        getInstance().workerHandler.post(work);
    }

    public static synchronized  void doWorkOnMainThread(Runnable work)
    {
        if (work != null)
            getInstance().uiHandler.post(work);
    }

    private Handler workerHandler;
    private static Worker theWorker = null;
    private static Handler uiHandler = null;
    private static synchronized Worker getInstance()
    {
        if (theWorker == null)
        {
            theWorker = new Worker();
            theWorker.start();
            theWorker.waitUntilReady();
        }
        return theWorker;
    }
    private Worker() {
        super("WorkerThread");
        uiHandler = new Handler(Looper.getMainLooper());
    }
    private synchronized void waitUntilReady() {
        workerHandler = new Handler(getLooper());
    }
}
