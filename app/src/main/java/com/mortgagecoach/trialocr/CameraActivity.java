package com.mortgagecoach.trialocr;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.hardware.Camera;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import java.io.FileOutputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@TargetApi(14)
public class CameraActivity extends ActionBarActivity implements Camera.PictureCallback
{
    private Camera mCamera = null;
    private CameraPreview mPreview = null;

    private int useCameraId = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            ActionBar actionBar = getActionBar();
            if (actionBar != null) actionBar.hide();
        }
        setContentView(R.layout.activity_camera);

        getSupportActionBar().hide();

        //check number of cameras, if greater than 2, set front camera as default
        int numCameras = Camera.getNumberOfCameras();
        if (numCameras < 2) //only one camera, use camera Id = 0
        {
            useCameraId = 0;
            mCamera = Camera.open(useCameraId);
        }
        else
        {
            //there are at least two cameras, try to choose the back facing camera
            //if there is no back facing camera, use camera 0.
            useCameraId = 0;
            Camera.CameraInfo info = new Camera.CameraInfo();
            for(int i=0; i<numCameras; i++)
            {
                Camera.getCameraInfo(i, info);
                if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
                {
                    useCameraId = i;
                    break;
                }
            }
            mCamera = Camera.open(useCameraId);
        }

        ImageButton recording = (ImageButton) findViewById(R.id.toggleRecordingButton);
        recording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> focusModes = mCamera.getParameters().getSupportedFocusModes();
                if (focusModes != null &&focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO))
                {
                    mCamera.autoFocus(new Camera.AutoFocusCallback() {
                        @Override
                        public void onAutoFocus(boolean success, Camera camera) {
                            mCamera.takePicture(null, null, CameraActivity.this);
                        }
                    });
                }
                else
                {
                    mCamera.takePicture(null, null, CameraActivity.this);
                }
            }
        });

        setupNewCamera();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        releaseOldCamera();
        mPreview = null;
        //whenever the activity pauses, also finish it and
        //force the user to restart the recording.
        setResult(RESULT_CANCELED);
        finish();
    }

    private void releaseOldCamera()
    {
        if (mCamera != null)
        {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    private void setupNewCamera()
    {
        FrameLayout previewParent = (FrameLayout) findViewById(R.id.camera_preview);
        //release the existing surface
        previewParent.removeAllViews();

        if (mCamera != null)
        {
            Camera.Parameters params = mCamera.getParameters();
            Camera.Size optimalSize = chooseOptimalPictureSize();
            Camera.Size previewSize = findBestPreviewSize(optimalSize);
            if (params != null )
            {
                android.hardware.Camera.CameraInfo info =
                        new android.hardware.Camera.CameraInfo();
                android.hardware.Camera.getCameraInfo(useCameraId, info);
                int rotation = this.getWindowManager().getDefaultDisplay()
                        .getRotation();
                Log.d("SVK", "camera orientation=" + info.orientation + ",display=" + rotation);

                params.setPictureSize(optimalSize.width, optimalSize.height);
                params.setPreviewSize(previewSize.width, previewSize.height);
                params.setRotation(90);
                List<String> focusModes= params.getSupportedFocusModes();
                List<String> sceneModes = params.getSupportedSceneModes();
                List<String> colorEffects = params.getSupportedColorEffects();
                if (focusModes != null)
                {
                    if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO))
                    {
                        params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                    }
                }
                if (sceneModes != null && sceneModes.contains(Camera.Parameters.SCENE_MODE_BARCODE))
                {
                    params.setSceneMode(Camera.Parameters.SCENE_MODE_BARCODE);
                }
                if (colorEffects != null && colorEffects.contains(Camera.Parameters.EFFECT_WHITEBOARD))
                {
                    params.getSupportedColorEffects().contains(Camera.Parameters.EFFECT_WHITEBOARD);
                }
                mCamera.setParameters(params);
            }

            mPreview = new CameraPreview(CameraActivity.this, mCamera);
            previewParent.addView(mPreview);
        }

    }

    private Camera.Size chooseOptimalPictureSize()
    {
        //ideal picture size is 2400x3300 which is 8inx11in paper at 300 dpi.
        //so we want to sort the list by the differential in width to find the closes
        //matching size and for matching widths, sort by aspect ratio - 11/8 aspect ratio
        //to find the closes aspect ratio. choose the first item on the list
        List<Camera.Size> sizes = mCamera.getParameters().getSupportedPictureSizes();
        Collections.sort(sizes, new Comparator<Camera.Size>() {
            @Override
            public int compare(Camera.Size lhs, Camera.Size rhs) {
                //same width, sort by aspect ratio
                float aspect1 = Math.abs(1.0f * lhs.width / lhs.height - 11.0f / 8.0f);
                float aspect2 = Math.abs(1.0f * rhs.width / rhs.height - 11.0f / 8.0f);
                if (Math.abs(aspect1-aspect2) < 0.01)
                {
                    //use rhs on left to get biggest resolution first
                    return rhs.width * rhs.height - lhs.width * lhs.height;
                }
                else
                {
                    return Float.compare(aspect1, aspect2);
                }
            }
        });
        return sizes.get(0);
    }

    private Camera.Size findBestPreviewSize(Camera.Size pictureSize)
    {
        final float pictureAspect = 1.0f * pictureSize.width/pictureSize.height;
        //we want to find the preview sizes by to find the aspect ratio closest
        //to the one picture size and then choose the biggest resolution available
        List<Camera.Size> previewSizes = mCamera.getParameters().getSupportedPreviewSizes();
        Collections.sort(previewSizes, new Comparator<Camera.Size>() {
            @Override
            public int compare(Camera.Size lhs, Camera.Size rhs) {
                //same width, sort by aspect ratio
                float aspect1 = Math.abs(1.0f * lhs.width / lhs.height - pictureAspect);
                float aspect2 = Math.abs(1.0f * rhs.width / rhs.height - pictureAspect);
                if (Math.abs(aspect1 - aspect2) < 0.01) //identical
                {
                    //use rhs on left to get biggest resolution first
                    return rhs.width * rhs.height - lhs.width * lhs.height;
                } else {
                    return Float.compare(aspect1, aspect2);
                }
            }
        });
        return previewSizes.get(0);
    }

    public void onPictureTaken(byte[] photoData, Camera camera)
    {
        if (photoData == null) return;
        Log.d("SVK", "jpeg photo bytes = " + photoData.length);
        try
        {
            FileOutputStream fos = new FileOutputStream(MainActivity.DATA_PATH + "/ocr.jpg");
            fos.write(photoData);
            fos.close();
        }
        catch (Exception e)
        {
            return;
        }
        setResult(RESULT_OK);
        finish();
    }
}
